package com.example.conversion;

import  androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {
    private Spinner spnconversion;
    private Button btncerrar;
    private Button btnlimpiar;
    private EditText lblmoneda;
    private Button btncalcular;
    private TextView lblresultado;
    private  long opcion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        spnconversion=(Spinner) findViewById(R.id.spnconversion);
        btncerrar=(Button) findViewById(R.id.btncerrar);
        btnlimpiar=(Button) findViewById(R.id.btnlimpiar);
        lblmoneda=(EditText) findViewById(R.id.lblmoneda);
        btncalcular=(Button) findViewById(R.id.btncalcular);
        lblresultado=(TextView) findViewById(R.id.lblresultado);
        final ArrayAdapter<String> Adaptador=new
                ArrayAdapter<String>(MainActivity.this,android.R.layout.simple_expandable_list_item_1,
                getResources().getStringArray(R.array.conversion));

//con esto jala el boton cerrar
    btncerrar.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    });
// pa que quede limpiesito
     btnlimpiar.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
            lblmoneda.setText("");
            lblresultado.setText("");
             spnconversion.setAdapter(Adaptador);

         }
     });
//boto pa calcualar aca bien perron
        btncalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ciclo para que no truene el programa al hacer click con nada puesto
                if(lblmoneda.getText().toString().matches(""))
                    Toast.makeText(MainActivity.this,"falto capturar numero",
                            Toast.LENGTH_SHORT).show();
                else {
                    float resultado = 0.0f;
                    float cantidad = Float.parseFloat(lblmoneda.getText().toString());

                    switch ((int) opcion) {

                        case 0:
                            resultado = cantidad * 0.05f;
                            break;
                        case 1:
                            resultado = cantidad * 0.048f;
                            break;
                        case 2:
                            resultado = cantidad * 0.07f;
                            break;
                        case 3:
                            resultado = cantidad * 0.41f;
                            break;
                    }

                    lblresultado.setText(String.valueOf(resultado));

                }

            }
        });

//esta wea sirve para que  jala el Spinner


        spnconversion.setAdapter(Adaptador);
        spnconversion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long L) {
                    opcion=adapterView.getItemIdAtPosition(i);
                    lblresultado.setText("");


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}
